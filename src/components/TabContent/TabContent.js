import React from 'react'

export default function TabContent(props) {
  return (
    <div>
        <div
            className={`tab-pane fade ${props.show}`}
            id={props.id}
            role="tabpanel"
            aria-labelledby={`nav-${props.id}-tab`}
            style= {{
              // display:'none'
            }}
          >
                {props.children}
          </div>
    </div>
  )
}
