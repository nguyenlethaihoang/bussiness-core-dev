import React from 'react'

export default function TabChangeNonActive(props) {
  return (
    <div
      className="tab-pane fade"
      id={`${props.id}`}
      role="tabpanel"
      // aria-labelledby="nav-home-tab"
    >
        {props.children}
    </div>
  );
}
