import React from 'react'

export default function deleteIcon() {
  return (
    <div>
           <svg width="18" height="18" viewBox="0 0 24 24" style={{fontSize: '16px'}} xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd"><path d="M7 2c1.695 1.942 2.371 3 4 3h13v17h-24v-20h7zm4 5c-2.339 0-3.537-1.388-4.917-3h-4.083v16h20v-13h-11zm-1.586 3l2.586 2.586 2.586-2.586 1.414 1.414-2.586 2.586 2.586 2.586-1.414 1.414-2.586-2.586-2.586 2.586-1.414-1.414 2.586-2.586-2.586-2.586 1.414-1.414z" fill="white"/></svg>
    </div>
  )
}
