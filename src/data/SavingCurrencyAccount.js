const SavingCurrencyAccount = [
    { "id": 0, "name": ''},
    { "id": 1, "name": 'EUR-30001-2054-3210'},
    { "id": 2, "name": 'GBP-40001-2054-4242'},
    { "id": 3, "name": 'JPY-50001-2054-6428'},
    { "id": 4, "name": 'USD-20001-2054-1221'},
    { "id": 5, "name": 'VND-10001-0695-1221'},
  ];

  export default SavingCurrencyAccount;