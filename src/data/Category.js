const Category = [
    { "id": 0, "name": ''},
    { "id": 1, "name": '1000 - Tiền gửi thanh toán'},
    { "id": 2, "name": '2000 - Tiết kiệm không kỳ hạn'},
    { "id": 3, "name": '3000 - Tiết kiệm có kỳ hạn'},
    { "id": 4, "name": '3001 - Tiền gửi có kỳ hạn'},
    { "id": 5, "name": '5000 - Tiết kiệm lãi trả trước'}
  ];

  export default Category;