const AccountType = [
    { "id": 1, "name": 'Current & Non-Term Saving Account'},
    { "id": 2, "name": 'Saving Account - Arrear'},
    { "id": 3, "name": 'Saving Account - Periodic'},
    { "id": 4, "name": 'Saving Account - Discounted'},
    { "id": 5, "name": 'Loan Working Account'},
  ];

  export default AccountType;