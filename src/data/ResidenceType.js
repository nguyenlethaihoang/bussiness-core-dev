const ResidenceType = [
    { "id": 0, "name": '' },
    { "id": 1, "name": 'FARM.HOUSE' },
    { "id": 2, "name": 'INDEPENDENT.HOUSE' },
    { "id": 3, "name": 'RESIDENTIAL.APT' },
    { "id": 4, "name": 'SERVICED.APT' },
    { "id": 5, "name": 'OTHER' }
  ];

  export default ResidenceType;